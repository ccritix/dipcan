import { ItemEventData } from "ui/list-view"
import { Page } from "ui/page";
import { ios } from "application";
import { Component, OnInit, NgZone} from "@angular/core";
import { Button } from "tns-core-modules/ui/button/button";
import { Slider } from "tns-core-modules/ui/slider/slider";
import { setInterval, clearInterval } from "tns-core-modules/timer";
import { calcBindingFlags } from "@angular/core/src/view/util";
require('nativescript-websockets');
var timer = require('timer');

declare var UITableViewCellSelectionStyle;

class InfoClass {
    constructor(public name: string, public value: string) { }
}

let infoIconNumber = 8;

let InfoNameArray = ["Engine load [%]", "Coolant temperature [C]", "Manifold pressure [kPa]", "Engine RPM [1/min]", "Speed [km/h]", "Intake air temperature [C]", "MAF rate [g/sec]", "Fuel gauge pressure [bar]", "Ambient led brightness", "Voltage [V]", "Increment"];
let InfoValueArray = ["None", "None", "None","None","None","None","None","None","None","None", "None"];

@Component({
    selector: "Home",
    moduleId: module.id,
    templateUrl: "./home.component.html",
    styleUrls: ['./home.component.css'],
})
export class HomeComponent implements OnInit {
    public pageName : string;
    public buttonUp;
    public buttonDown;
    public buttonLeft;
    public buttonRight;
    public rpmValue : number;
    public speedValue : number;

    public isSocketConnected : boolean;
    public isConnecting : boolean;

    public infoIconTurningLight:string;
    public infoIconClutch:string;
    public infoIconBrakes:string;
    
    public infoIconArray : boolean[] = [];
    public doorOpenArray : boolean[] = [];
    public isDoorsLocked:boolean;
    
    private timerId;
    private socket: any;    
    public infoArray: Array<InfoClass>;

    public FaultCodesArray = ["None"];

    onItemTap(args: ItemEventData): void {
    }

    constructor(
        private page: Page,
        private zone: NgZone,
    ) {
        for (let i = 0; i < infoIconNumber; i++){
            this.infoIconArray[i] = false;
        }
        this.infoIconArray[0] = this.infoIconArray[infoIconNumber-1] = true;

        this.isDoorsLocked = true;
        for (let i = 0; i < 4; i++){
            this.doorOpenArray[i] = true;
        }

        this.buttonUp = String.fromCharCode(0xea3a);
        this.buttonDown = String.fromCharCode(0xea3e);
        this.buttonLeft = String.fromCharCode(0xea40);
        this.buttonRight = String.fromCharCode(0xea3c);
        

        this.speedValue = 0;
        this.rpmValue = 0;

        this.pageName = "LiveInfo";
        this.isSocketConnected = false;
        this.isConnecting = true;
        this.socket = new WebSocket("ws://192.168.4.1:81", []);

        this.page.actionBarHidden = true;
        this.infoArray = [];
        for (let i = 0; i < InfoNameArray.length; i++) {
            this.infoArray.push(new InfoClass(InfoNameArray[i], InfoValueArray[i]));
        }

    }

    public ngOnInit() {
        this.socket.addEventListener('open', event => {
            this.zone.run(() => {
                //this.messages.push({content: "Welcome to the chat!"});
                this.isSocketConnected = true;
                this.isConnecting = false;
                console.log("Opened!");
            });
        });
        this.socket.addEventListener('message', event => {
            this.zone.run(() => {
                //console.log("Received packet!");
                //this.messages = JSON.stringify(event.data);
                let data =  new Uint8Array(event.data);
                this.infoArray = [];

                this.infoArray.push(new InfoClass(InfoNameArray[0], String(data[1])));
                this.infoArray.push(new InfoClass(InfoNameArray[1], String(data[2])));
                this.infoArray.push(new InfoClass(InfoNameArray[2], String(data[3])));
                this.infoArray.push(new InfoClass(InfoNameArray[3], String(data[4]  | (data[5] << 8))));
                this.infoArray.push(new InfoClass(InfoNameArray[4], String(data[6])));
                this.infoArray.push(new InfoClass(InfoNameArray[5], String(data[7])));
                this.infoArray.push(new InfoClass(InfoNameArray[6], String(data[8]  | (data[9] << 8))));
                this.infoArray.push(new InfoClass(InfoNameArray[7], String(data[10] | (data[11] << 8))));
                this.infoArray.push(new InfoClass(InfoNameArray[8], String(data[12])));
                this.infoArray.push(new InfoClass(InfoNameArray[9], String((data[20] / 10.0).toFixed(1))));
                this.infoArray.push(new InfoClass(InfoNameArray[10], String(data[13])));

                this.doorOpenArray[0]   = data[14] & 0x01 ? true : false;
                this.doorOpenArray[1]   = data[14] & 0x02 ? true : false;
                this.doorOpenArray[2]   = data[14] & 0x04 ? true : false;
                this.doorOpenArray[3]   = data[14] & 0x08 ? true : false;
                this.isDoorsLocked      = data[14] & 0x10 ? true : false;

                this.infoIconArray[0]   = data[15] & 0x10 ? true : false;

                this.infoIconArray[1]   = data[16] & 0x01 ? true : false;
                this.infoIconArray[2]   = data[16] & 0x02 ? true : false;
                this.infoIconArray[3]   = data[16] & 0x04 ? true : false;

                this.infoIconArray[4]   = data[15] & 0x01 ? true : false;
                this.infoIconArray[5]   = data[15] & 0x02 ? true : false;
                this.infoIconArray[6]   = data[15] & 0x04 ? true : false;
                this.infoIconArray[7]   = data[15] & 0x20 ? true : false;

                this.rpmValue = data[4]  | (data[5] << 8);
                this.speedValue = data[6];

                if ( (data[17] & data[18] & data[19]) != 0xff){
                    if ( (data[17] | data[18] | data[19]) == 0x00){
                        console.log("fault code reading finished");
                    } else {
                        let str = "";
                        switch (data[17]>>6){
                            case 0: str += 'P'; break;
                            case 1: str += 'C'; break;
                            case 2: str += 'B'; break;
                            case 3: str += 'U'; break;
                        }
                        str += ((data[17] >> 4) & 0x03).toString(16);;
                        str += (data[17] & 0x0f).toString(16);
                        str += (data[18] >> 4).toString(16);;
                        str += (data[18] & 0x0f).toString(16);;
                        str += '-';
                        str += data[19].toString(16);
                        this.FaultCodesArray.push(str.toUpperCase());
                        console.log(str.toUpperCase());
                    }
                }
                /*
                for (let i = 3; i < InfoNameArray.length; i++) {
                    //this.infoArray[i].value = String(data[i]);
                    this.infoArray.push(new InfoClass(InfoNameArray[i], String(data[i + 4])));
                }*/
            });
        });
        this.socket.addEventListener('close', event => {
            this.zone.run(() => {
                //this.messages.push({content: "You have been disconnected"});
                this.isSocketConnected = false;
                this.isConnecting = false;
                console.log("Close!");
            });
        });
        this.socket.addEventListener('error', event => {
            console.log("The socket had an error");
        });
    }
  
    public ngOnDestroy() {
        this.socket.close();
    }
  /*
    public send() {
        this.socket.send(this.txBuffer);
    }*/

    onItemLoading(args: any) {
        if (ios) {
            const cell = args.ios;
            cell.selectionStyle = UITableViewCellSelectionStyle.UITableViewCellSelectionStyleNone;
        }
    }

    tabSelected(args: number) {
        switch (args) {
            case 0: this.pageName = "LiveInfo"; break;
            case 1: this.pageName = "Radio"; break;
            case 2: this.pageName = "Windows"; break;
            case 3: this.pageName = "Mirrors"; break;
            case 4: this.pageName = "Services"; break;
            default: break;
        }
        // console.log("tab selected: " + args);
    }

    public onTap(args){
        let button = <Button>args.object;
        if (button.id == "skip"){ this.isSocketConnected = true; }
        if (button.id == "reconnect") { this.isConnecting = true; this.socket = new WebSocket("ws://192.168.4.1:81", []); this.ngOnInit() }
    }

    public onTouch(args){
        if (args.action == "up"){
            timer.clearInterval(this.timerId);
            return;
        }
        
        if (args.action == "down"){
            let button = <Button>args.object;
            let txBuffer = new Uint8Array(17);
            txBuffer[0] = "[".charCodeAt(0);
            txBuffer[16] = ']'.charCodeAt(0);

            //console.log(button.id);
            if (button.id == "volDOWN"){        txBuffer[1] = 1;        }
            if (button.id == "volUP"){          txBuffer[2] = 1;        }
            if (button.id == "songNEXT"){       txBuffer[3] = 1;        } 
            if (button.id == "songPREV"){       txBuffer[4] = 1;        }
            if (button.id == "menuNEXT"){       txBuffer[6] = 1;        }
            if (button.id == "menuPREV"){       txBuffer[5] = 1;        }
            if (button.id == "SEL"){            txBuffer[7] = 1;        }
            if (button.id == "MENU"){           txBuffer[8] = 1;        }

            if (button.id == "RLWindowUp"){     txBuffer[9] = 32;       }
            if (button.id == "RLWindowDown"){   txBuffer[9] = 64;       }
            if (button.id == "RRWindowUp"){     txBuffer[9] = 2;        }
            if (button.id == "RRWindowDown"){   txBuffer[9] = 4;        }
            if (button.id == "FLWindowUp"){     txBuffer[10] = 8; }//32;        }
            if (button.id == "FLWindowDown"){   txBuffer[10] = 16; }//;        }
            if (button.id == "FRWindowUp"){     txBuffer[10] = 9; }//2;        }
            if (button.id == "FRWindowDown"){   txBuffer[10] = 17; }//4;        }

            if (button.id == "LMirrorUp"){      txBuffer[11] = 4;       }
            if (button.id == "LMirrorDown"){    txBuffer[11] = 8;       }
            if (button.id == "LMirrorLeft"){    txBuffer[11] = 1;       }
            if (button.id == "LMirrorRight"){   txBuffer[11] = 2;       }
            if (button.id == "RMirrorUp"){      txBuffer[12] = 4;       }
            if (button.id == "RMirrorDown"){    txBuffer[12] = 8;       }
            if (button.id == "RMirrorLeft"){    txBuffer[12] = 1;       }
            if (button.id == "RMirrorRight"){   txBuffer[12] = 2;       }
            if (button.id == "dashboardTest"){  txBuffer[13] = 1;       }
            if (button.id == "interiorLight"){  txBuffer[14] = 1;       }
            if (button.id == "getFaultCodes"){  this.FaultCodesArray = []; 
                                                txBuffer[15] = 1;       }
            
            this.socket.send(txBuffer); 

            //console.log(Array.apply([], txBuffer).join(","));

            this.timerId = timer.setInterval(() => {
                this.socket.send(txBuffer);
                //console.log(Array.apply([], txBuffer).join(","));
            }, 250);            
        }
    }   

    public onSliderValueChanged(args){
        let slider = <Slider>args.object;
        //this.txBuffer[12] = slider.value;
        //this.brightness[0] = slider.value;
        console.log(slider.value);
    }
}
