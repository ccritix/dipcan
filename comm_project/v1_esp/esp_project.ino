#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <WebSocketsServer.h>
#include "dc_wifi_packet_descriptor.h"
//-------------------------------------------------------------------------------------------------------------------------
WebSocketsServer webSocket = WebSocketsServer(81);
const int LEDpin = 5;
const uint8_t CLIENT = 0;
bool isClientConnected = false;
const char *ssid = "dipCAN"; // The name of the Wi-Fi network that will be created
const char *password = "thereisnospoon";   // The password required to connect to it, leave blank for an open network
//-------------------------------------------------------------------------------------------------------------------------
unsigned long timeMs;
unsigned long ledTime;
//-------------------------------------------------------------------------------------------------------------------------
uint8_t demoInfoPacket[INFO_PACKET_LEN];
uint8_t demoControlPacket[CTRL_PACKET_LEN];
uint16_t increment = 0;
//-------------------------------------------------------------------------------------------------------------------------
void webSocketEvent(uint8_t num, WStype_t type, uint8_t * payload, size_t length) {
    switch(type) {
        case WStype_DISCONNECTED: {
          isClientConnected = false;
          Serial.printf("[%u] Disconnected!\n", num);
        } break;
        
        case WStype_CONNECTED: {
          isClientConnected = true;
          increment = 0;
          IPAddress ip = webSocket.remoteIP(num);
          Serial.printf("[%u] Connected from %d.%d.%d.%d url: %s\n", num, ip[0], ip[1], ip[2], ip[3], payload);
       
          // send message to client
          webSocket.sendTXT(CLIENT, "Connected");
        } break;

        case WStype_BIN:
        case WStype_TEXT:
            Serial.write(payload, length);

            // send message to client
            //webSocket.sendTXT(CLIENT, "message here");

            // send data to all connected clients
            // webSocket.broadcastTXT("message here");
        break;
    }
}
//-------------------------------------------------------------------------------------------------------------------------
void setup() {
    Serial.begin(115200);
    delay(1000);

    timeMs = millis();
    ledTime = millis();
    WiFi.softAP(ssid, password);             // Start the access point
    webSocket.begin();
    webSocket.onEvent(webSocketEvent);
    Serial.println("started");
    pinMode(LEDpin, OUTPUT);
}
//-------------------------------------------------------------------------------------------------------------------------
void sendDemoInfoPacket(void){
  /*
  demoInfoPacket[INFO_PACKET_START] = INFO_PACKET_START_CHAR;
  demoInfoPacket[INFO_PACKET_END] = INFO_PACKET_END_CHAR;
  demoInfoPacket[INFO_INCREMENT] = increment & 0xff;
  demoInfoPacket[INFO_INCREMENT+1] = increment >> 8;
  demoInfoPacket[INFO_SPEED] = increment & 0xff;
  demoInfoPacket[INFO_SPEED+1] = increment >> 8;
  demoInfoPacket[INFO_REV] = increment & 0xff;
  demoInfoPacket[INFO_REV+1] = increment >> 8;
  demoInfoPacket[INFO_HANDBRAKE] = 1;
  demoInfoPacket[INFO_FRONT_LEFT_DOOR] = 1;
  demoInfoPacket[INFO_FRONT_RIGHT_DOOR] = 1;
  demoInfoPacket[INFO_REAR_LEFT_DOOR] = 0;
  demoInfoPacket[INFO_REAR_RIGHT_DOOR] = 0;
  demoInfoPacket[INFO_FUEL] = 50;
  webSocket.sendBIN(CLIENT, demoInfoPacket, INFO_PACKET_LEN);
  */
  //Serial.println("sending...");
}
//-------------------------------------------------------------------------------------------------------------------------
void sendDemoControlPacket(void){
  /*
  demoControlPacket[CTRL_PACKET_START] = CTRL_PACKET_START_CHAR;
  demoControlPacket[CTRL_LED1] = (increment >> 0) & 0x01;
  demoControlPacket[CTRL_LED2] = (increment >> 1) & 0x01;
  demoControlPacket[CTRL_LED3] = (increment >> 2) & 0x01;
  demoControlPacket[CTRL_PACKET_END] = CTRL_PACKET_END_CHAR;

  Serial.write(demoControlPacket, CTRL_PACKET_LEN);
  */
}
//-------------------------------------------------------------------------------------------------------------------------
uint8_t rxBuffer[256];
uint8_t rxPtr = 0;
int gpioState = HIGH;

void loop() {
    webSocket.loop();
    if (Serial.available() > 0) {
      rxBuffer[rxPtr] = Serial.read();
      if (  (rxBuffer[rxPtr] == INFO_PACKET_END_CHAR) && 
            (rxPtr > INFO_PACKET_DATA_LEN) &&
            (rxBuffer[rxPtr-INFO_PACKET_DATA_LEN - 1] == INFO_PACKET_START_CHAR)){              
            if (isClientConnected){
                webSocket.sendBIN(CLIENT, &rxBuffer[rxPtr-INFO_PACKET_DATA_LEN-1], INFO_PACKET_LEN);
            }
            digitalWrite(LEDpin, gpioState);
            gpioState = gpioState == HIGH ? LOW : HIGH;
            rxPtr = 0;
      } else {
        rxPtr++;
      }
    }    
//     digitalWrite(LED_BUILTIN, HIGH);
//     delay(50);
//     digitalWrite(LED_BUILTIN, LOW);
      //delay(500);
      //sendDemoControlPacket();
}

