#include "dc_usb.h"
#include "main.h"
#include "string.h"
#include "usbd_cdc_if.h"
#include "queue.h"
#include "portmacro.h"
#include "dc_wifi.h"
#include "dc_controller.h"
//---------------------------------------------------------------------------------------------------------------------
QueueHandle_t dc_usb_txBufferQueue;
QueueHandle_t dc_usb_rxBufferQueue;
extern USBD_HandleTypeDef hUsbDeviceFS;
//---------------------------------------------------------------------------------------------------------------------
void dc_usb_init(void){
	HAL_GPIO_WritePin(USB_EN_GPIO_Port, USB_EN_Pin, GPIO_PIN_SET);

	dc_usb_txBufferQueue = xQueueCreate( DC_USB_TX_QUEUE_SIZE, sizeof(dc_usb_packet_t));
	dc_usb_rxBufferQueue = xQueueCreate( DC_USB_RX_QUEUE_SIZE, sizeof(dc_usb_packet_t));
}
//---------------------------------------------------------------------------------------------------------------------
void dc_usb_sendStr(char * cPtr){
	dc_usb_packet_t txPacket;
	txPacket.len = strlen(cPtr) > DC_USB_TX_PACKET_SIZE ? DC_USB_TX_PACKET_SIZE : strlen(cPtr);
	memcpy(txPacket.data, (uint8_t*)cPtr, txPacket.len);

	dc_usb_sendData(&txPacket);
}
//---------------------------------------------------------------------------------------------------------------------
void dc_usb_sendData(const dc_usb_packet_t * txPacket){
	if (inHandlerMode()){
		BaseType_t xHigherPriorityTaskWoken = pdFALSE;
		if ( xQueueSendFromISR( dc_usb_txBufferQueue, txPacket, &xHigherPriorityTaskWoken) != pdTRUE ){
			// error
		}
		portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
	} else {
		if ( xQueueSend(dc_usb_txBufferQueue, txPacket, 50) != pdTRUE ){
			// error
		}
	}
}
//---------------------------------------------------------------------------------------------------------------------
void dc_usb_txTask(void * pvParameters){
	const uint16_t USB_TX_BUFFER_SIZE = 256;
	dc_usb_packet_t tx_packet;
	volatile uint32_t failCounter = 0;
	uint8_t txDualBuffer[USB_TX_BUFFER_SIZE * 2];
	uint16_t txPtr = 0, tempPtr = 0;

	for(;;){
		if ( xQueueReceive(dc_usb_txBufferQueue, &tx_packet, portMAX_DELAY) == pdTRUE ){

			if (USB_TX_USE_BUFFER){
				if (tx_packet.len > USB_TX_BUFFER_SIZE){
					Error_Handler();
				}

				if ( txPtr + tx_packet.len > USB_TX_BUFFER_SIZE * 2 ){
					tempPtr = 0;
					while (CDC_Transmit_FS(&(txDualBuffer[USB_TX_BUFFER_SIZE]),  txPtr-USB_TX_BUFFER_SIZE) == USBD_BUSY){
						failCounter++;
					}
				} else if ( txPtr < USB_TX_BUFFER_SIZE && txPtr + tx_packet.len > USB_TX_BUFFER_SIZE){
					tempPtr = USB_TX_BUFFER_SIZE;
					while (CDC_Transmit_FS(txDualBuffer,  txPtr) == USBD_BUSY){
						failCounter++;
					}
				} else {
					tempPtr = txPtr;
				}

				memcpy(txDualBuffer + tempPtr, tx_packet.data, tx_packet.len);
				txPtr = tempPtr + tx_packet.len;
			} else {
				while (CDC_Transmit_FS(tx_packet.data,  tx_packet.len) == USBD_BUSY){
					failCounter++;
				}
			}
		}
	}
}
//---------------------------------------------------------------------------------------------------------------------
void dc_usb_rxTask(void * pvParameters){
	dc_usb_packet_t rx_packet;
	for(;;){
		if ( xQueueReceive(dc_usb_rxBufferQueue, &rx_packet, portMAX_DELAY) == pdTRUE ){
			if (dc_controller_processControlPacket(rx_packet.data, rx_packet.len, CTRL_SRC_USB)) { continue; }
			if (dc_wifi_isBootloaderMode()){
				dc_wifi_directTx(rx_packet.data, rx_packet.len);
			}
		}
	}
}
//---------------------------------------------------------------------------------------------------------------------
void dc_usb_rxHandler(uint8_t * pData, uint32_t uLen){
	dc_usb_packet_t rx_packet;
	if (uLen > DC_USB_TX_PACKET_SIZE){
		Error_Handler();
	}

	memcpy(rx_packet.data, pData, uLen);
	rx_packet.len = uLen;

	if (inHandlerMode()){
		BaseType_t xHigherPriorityTaskWoken = pdFALSE;
		if ( xQueueSendFromISR( dc_usb_rxBufferQueue, &rx_packet, &xHigherPriorityTaskWoken) != pdTRUE ){
			// error
		}
		portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
	} else {
		if ( xQueueSend(dc_usb_rxBufferQueue, &rx_packet, 50) != pdTRUE ){
			// error
		}
	}
}
//---------------------------------------------------------------------------------------------------------------------
