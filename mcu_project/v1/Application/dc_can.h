#ifndef DC_CAN_H_
#define DC_CAN_H_
//---------------------------------------------------------------------------------------------------------------------
#include "stm32f4xx.h"
//---------------------------------------------------------------------------------------------------------------------
extern CAN_HandleTypeDef  * canHandles[];
//---------------------------------------------------------------------------------------------------------------------
#define DC_CAN_TX_BUFFER_SIZE	50

typedef struct{
	CAN_HandleTypeDef * canHandle;
	uint32_t 	ID;
	uint8_t		DLC;
	uint8_t		data[8];
} dc_can_tx_packet_t;

typedef enum{
	CAN_H = 0,
	CAN_M,
	CAN_L,
	CAN_MAX_CH,
} dc_can_ch_t;
//---------------------------------------------------------------------------------------------------------------------
void dc_can_init(void);
void dc_can_rxTask( void * pvParameters);
void dc_can_txTask(void * pvParameters);
void dc_can_rx_handler(CAN_HandleTypeDef *hcan);
void dc_can_sendPacket(CAN_HandleTypeDef * canHandle, uint32_t id, uint8_t dlc, uint8_t * pData);

#endif /* DC_CAN_H_ */
