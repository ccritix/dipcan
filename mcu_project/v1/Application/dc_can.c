#include "dc_can.h"
#include "main.h"
#include "dc_main.h"
#include "dc_usb.h"
#include "stm32f4xx.h"
#include "FreeRTOS.h"
#include "task.h"
#include <string.h>
#include "dc_packet_processor.h"
#include "dc_controller.h"
//---------------------------------------------------------------------------------------------------------------------
extern TaskHandle_t dc_can_hsCanRxTaskHandle;
extern TaskHandle_t dc_can_msCanRxTaskHandle;
extern TaskHandle_t dc_can_lsCanRxTaskHandle;

extern CAN_HandleTypeDef hcan1;
extern CAN_HandleTypeDef hcan2;
extern CAN_HandleTypeDef hcan3;

CAN_HandleTypeDef * canHandles[] = { &hcan1, &hcan2, &hcan3 };
//---------------------------------------------------------------------------------------------------------------------
QueueHandle_t dc_can_txBufferQueue;
//---------------------------------------------------------------------------------------------------------------------
typedef struct{
	bool 		isActive;
	uint8_t 	rxPacketCount;
	TickType_t 	lastPacket;
} dc_can_status_t;

dc_can_status_t dc_can_status[CAN_MAX_CH];
//---------------------------------------------------------------------------------------------------------------------
void dc_lsCan_init(void){
	if (dc_MX_CAN3_Init() && HAL_CAN_Start(&hcan3) == HAL_OK){
		//if (HAL_CAN_Start(&hcan3) != HAL_OK) { Error_Handler(); }
		if (HAL_CAN_ActivateNotification(&hcan3, CAN_IT_TX_MAILBOX_EMPTY) != HAL_OK) { Error_Handler(); }
		if (HAL_CAN_ActivateNotification(&hcan3, CAN_IT_RX_FIFO0_MSG_PENDING) != HAL_OK) { Error_Handler(); }
		if (HAL_CAN_ActivateNotification(&hcan3, CAN_IT_RX_FIFO1_MSG_PENDING) != HAL_OK) { Error_Handler(); }

		dc_can_status[CAN_L].rxPacketCount = 0;
		dc_can_status[CAN_L].lastPacket = 0;

		dc_can_status[CAN_L].isActive = true;
		dc_usb_sendStr("LSCAN inited\r\n");

		HAL_GPIO_WritePin(LED2_GPIO_Port, LED2_Pin, GPIO_PIN_RESET);
	} else {
		dc_usb_sendStr("LSCAN init failed\r\n");
	}
}

void dc_lsCan_deInit(void){
	HAL_CAN_Stop(&hcan3);
	HAL_CAN_DeInit(&hcan3);
	dc_can_status[CAN_L].isActive = false;
}

void dc_can_init(void){
	if (HAL_CAN_Start(&hcan1) != HAL_OK) { Error_Handler();}
	if (HAL_CAN_ActivateNotification(&hcan1, CAN_IT_TX_MAILBOX_EMPTY) != HAL_OK) { Error_Handler(); }
	if (HAL_CAN_ActivateNotification(&hcan1, CAN_IT_RX_FIFO0_MSG_PENDING) != HAL_OK) { Error_Handler(); }
	if (HAL_CAN_ActivateNotification(&hcan1, CAN_IT_RX_FIFO1_MSG_PENDING) != HAL_OK) { Error_Handler(); }

	if (HAL_CAN_Start(&hcan2) != HAL_OK) { Error_Handler(); }
	if (HAL_CAN_ActivateNotification(&hcan2, CAN_IT_TX_MAILBOX_EMPTY) != HAL_OK) { Error_Handler(); }
	if (HAL_CAN_ActivateNotification(&hcan2, CAN_IT_RX_FIFO0_MSG_PENDING) != HAL_OK) { Error_Handler(); }
	if (HAL_CAN_ActivateNotification(&hcan2, CAN_IT_RX_FIFO1_MSG_PENDING) != HAL_OK) { Error_Handler(); }

	dc_lsCan_init();

	HAL_GPIO_WritePin(HS_CAN_RS_GPIO_Port, HS_CAN_RS_Pin, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(MS_CAN_RS_GPIO_Port, MS_CAN_RS_Pin, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(LS_CAN_RS_GPIO_Port, LS_CAN_RS_Pin, GPIO_PIN_RESET);

	dc_can_status[CAN_H].rxPacketCount = 0;
	dc_can_status[CAN_M].rxPacketCount = 0;

	dc_can_txBufferQueue = xQueueCreate( DC_CAN_TX_BUFFER_SIZE, sizeof(dc_can_tx_packet_t));
}
//---------------------------------------------------------------------------------------------------------------------
// Sending
//---------------------------------------------------------------------------------------------------------------------
void dc_can_sendPacket(CAN_HandleTypeDef * canHandle, uint32_t id, uint8_t dlc, uint8_t * pData){
	dc_can_tx_packet_t 	txPacket = {
			.canHandle 	= canHandle,
			.ID 		= id,
			.DLC		= dlc,
	};
	if (dlc <= 8){
		memcpy(txPacket.data, pData, dlc);
	}
	if (inHandlerMode()){
		BaseType_t xHigherPriorityTaskWoken = pdFALSE;
		if ( xQueueSendFromISR( dc_can_txBufferQueue, &txPacket, &xHigherPriorityTaskWoken) != pdTRUE ){
			Error_Handler();
		}
		portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
	} else {
		if ( xQueueSend(dc_can_txBufferQueue, &txPacket, 50) != pdTRUE ){
			//Error_Handler();
		}
	}
}
//---------------------------------------------------------------------------------------------------------------------
void dc_can_txTask(void * pvParameters){
	uint32_t           	txMailbox;
	dc_can_tx_packet_t 	txPacket;
	CAN_TxHeaderTypeDef	txHeader = {
			.ExtId = 0,
			.RTR = CAN_RTR_DATA,
			.IDE = CAN_ID_STD,
			.TransmitGlobalTime = DISABLE
	};

	for(;;){
		if ( xQueueReceive(dc_can_txBufferQueue, &txPacket, portMAX_DELAY) == pdTRUE ){
			txHeader.StdId = txPacket.ID;
			txHeader.DLC = txPacket.DLC;
			if (txPacket.canHandle == canHandles[CAN_L] && !dc_can_status[CAN_L].isActive){
				continue;
			}

			if (HAL_CAN_AddTxMessage(txPacket.canHandle, &txHeader, txPacket.data, &txMailbox) == HAL_OK){
				//dc_usb_send_str("sending...\r\n");
			} else {
				Error_Handler();
			}
		}
	}
}
//---------------------------------------------------------------------------------------------------------------------
// Receiving
//---------------------------------------------------------------------------------------------------------------------
void HAL_CAN_RxFifo0MsgPendingCallback(CAN_HandleTypeDef *hcan){
	BaseType_t xHigherPriorityTaskWoken = pdFALSE;

	HAL_CAN_DeactivateNotification(hcan, CAN_IT_RX_FIFO0_MSG_PENDING);

	if (hcan == canHandles[CAN_H]){
		vTaskNotifyGiveFromISR(dc_can_hsCanRxTaskHandle, &xHigherPriorityTaskWoken);
	}
	if (hcan == canHandles[CAN_M]){
		HAL_GPIO_TogglePin(LED3_GPIO_Port, LED3_Pin);
		vTaskNotifyGiveFromISR(dc_can_msCanRxTaskHandle, &xHigherPriorityTaskWoken);
	}
	if (hcan == canHandles[CAN_L]){

		vTaskNotifyGiveFromISR(dc_can_lsCanRxTaskHandle, &xHigherPriorityTaskWoken);
	}

	portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
}
//---------------------------------------------------------------------------------------------------------------------
void HAL_CAN_RxFifo1MsgPendingCallback(CAN_HandleTypeDef *hcan){
	Error_Handler();
	//dc_can_rxHandler(hcan);
}
//---------------------------------------------------------------------------------------------------------------------
void dc_can_rxTask( void * pvParameters){
	dc_can_ch_t 			dc_can_ch = (uint32_t) pvParameters;
	CAN_RxHeaderTypeDef   	rxHeader;
	uint8_t 				rxData[8];
	TickType_t 				delay = dc_can_ch == CAN_L ? pdMS_TO_TICKS(1000) : portMAX_DELAY;
	for(;;){
		dc_can_status[dc_can_ch].rxPacketCount = ulTaskNotifyTake(pdFALSE, delay) - 1;

		//dc_usb_sendStr("LSCAN init failed\r\n");
		if ( !dc_can_status[CAN_L].isActive){
			dc_lsCan_init();
			continue;
		}
		if (dc_can_status[CAN_L].lastPacket && (xTaskGetTickCount() - dc_can_status[CAN_L].lastPacket > pdMS_TO_TICKS(1500)) ){

			if (dc_can_status[CAN_L].isActive){
				HAL_GPIO_WritePin(LED2_GPIO_Port, LED2_Pin, GPIO_PIN_SET);
							dc_usb_sendStr("LSCAN deinited\r\n");
							dc_lsCan_deInit();
						}
						continue;
		}
/*
		if (dc_can_ch == CAN_L && (dc_can_status[CAN_L].rxPacketCount == 0 || dc_can_status[CAN_L].rxPacketCount == 0xff)){
			if (dc_can_status[CAN_L].isActive){
				dc_usb_sendStr("LSCAN deinited\r\n");
				dc_lsCan_deInit();
			}
			continue;
		}*/

		if (HAL_CAN_GetRxMessage( canHandles[dc_can_ch], CAN_RX_FIFO0, &rxHeader, rxData) == HAL_OK ) {
			HAL_CAN_ActivateNotification( canHandles[dc_can_ch], CAN_IT_RX_FIFO0_MSG_PENDING );

			if (dc_controller_fault_code_reader_callback(canHandles[dc_can_ch], &rxHeader, rxData)){
				continue;
			}

			switch (dc_can_ch){
				case CAN_H: dc_packet_processor_hs(&rxHeader, rxData); break;
				case CAN_M: dc_packet_processor_ms(&rxHeader, rxData); break;
				case CAN_L:
					dc_can_status[CAN_L].lastPacket = xTaskGetTickCount();
					dc_packet_processor_ls(&rxHeader, rxData); break;
			default: break;
			}

		} else {
			//Error_Handler();
		}
	}
}
//---------------------------------------------------------------------------------------------------------------------


