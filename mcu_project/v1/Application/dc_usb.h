#ifndef DC_USB_H_
#define DC_USB_H_
//---------------------------------------------------------------------------------------------------------------------
#include <stdint.h>
//---------------------------------------------------------------------------------------------------------------------
#define DC_USB_RX_QUEUE_SIZE 	10
#define DC_USB_TX_QUEUE_SIZE 	20
#define USB_TX_USE_BUFFER		0
#define DC_USB_TX_PACKET_SIZE 	200
#define INVALID_INDEX			0xFF
//---------------------------------------------------------------------------------------------------------------------
typedef struct{
	uint8_t data[DC_USB_TX_PACKET_SIZE];
	uint8_t len;
} dc_usb_packet_t;
//---------------------------------------------------------------------------------------------------------------------
void dc_usb_init(void);
void dc_usb_sendStr(char * cPtr);
void dc_usb_sendData(const dc_usb_packet_t * tx_packet);
void dc_usb_rxHandler(uint8_t * Buf, uint32_t Len);
void dc_usb_txTask(void * pvParameters);
void dc_usb_rxTask(void * pvParameters);
//---------------------------------------------------------------------------------------------------------------------
#endif /* DC_USB_H_ */
