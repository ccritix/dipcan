#include "dc_main.h"
#include "main.h"
#include "dc_usb.h"
#include "dc_can.h"
#include "usb_device.h"
#include "dc_wifi.h"
#include "dc_controller.h"
#include "dc_packet_processor.h"
//---------------------------------------------------------------------------------------------------------------------
TaskHandle_t dc_can_hsCanRxTaskHandle;
TaskHandle_t dc_can_msCanRxTaskHandle;
TaskHandle_t dc_can_lsCanRxTaskHandle;

TaskHandle_t dc_wifi_txTaskHandle;
//---------------------------------------------------------------------------------------------------------------------
void dc_initial_dashboard_test(void);
void dc_initTask( void *pvParameters ){
	MX_USB_DEVICE_Init();

	// peripheral init functions
	HAL_GPIO_WritePin(LED1_GPIO_Port, LED1_Pin, GPIO_PIN_SET);
	HAL_GPIO_WritePin(LED2_GPIO_Port, LED2_Pin, GPIO_PIN_SET);
	HAL_GPIO_WritePin(LED3_GPIO_Port, LED3_Pin, GPIO_PIN_SET);
	dc_usb_init();
	dc_wifi_init();
	dc_controller_init();
	dc_packet_processor_init();

	vTaskDelay(pdMS_TO_TICKS(500));
	// creating tasks
	xTaskCreate( dc_usb_txTask,		"usbTxTask", 	4096, NULL, 1, NULL );
	xTaskCreate( dc_usb_rxTask,		"usbRxTask", 	512, NULL, 3, NULL );

	xTaskCreate( dc_can_rxTask,		"hsCanRxTask", 	512, (void*)CAN_H, 2, &dc_can_hsCanRxTaskHandle );
	xTaskCreate( dc_can_rxTask, 	"lsCanRxTask", 	512, (void*)CAN_M, 2, &dc_can_msCanRxTaskHandle );
	xTaskCreate( dc_can_rxTask,		"msCanRxTask", 	512, (void*)CAN_L, 2, &dc_can_lsCanRxTaskHandle );

	xTaskCreate( dc_can_txTask, 	"canTxTask", 	configMINIMAL_STACK_SIZE, NULL, 1, NULL );

	xTaskCreate( dc_wifi_txTask,	"wifiTx", 256, NULL, 1, &dc_wifi_txTaskHandle);
	xTaskCreate( dc_wifi_rxTask,	"wifiRx", 256, NULL, 3, &dc_wifi_txTaskHandle);

	xTaskCreate( dc_controller_task, "control", 256, NULL, 3, NULL);

	dc_can_init();

	// hello world
	vTaskDelay(pdMS_TO_TICKS(500));
	dc_usb_sendStr("I am alive!\r\n");

	dc_initial_dashboard_test();

	vTaskDelete(NULL);
}

